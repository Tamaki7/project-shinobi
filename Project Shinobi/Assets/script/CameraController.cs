using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target; // el objeto que seguir� la c�mara
    public float smoothSpeed = 0.125f; // suavidad de movimiento de la c�mara
    public Vector3 offset; // distancia entre la c�mara y el objeto que sigue

    void FixedUpdate()
    {
        Vector3 desiredPosition = target.position + offset; // posici�n deseada de la c�mara
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed); // suavizado de movimiento de la c�mara
        transform.position = smoothedPosition; // actualizaci�n de la posici�n de la c�mara
        transform.LookAt(target); // apuntar la c�mara hacia el objeto que sigue
    }
}
