using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour

{
    public float moveSpeed = 5f; // Velocidad de movimiento del jugador
    public float turnSpeed = 10f; // Velocidad de giro del jugador
    public float jumpForce = 10f; // Fuerza de salto del jugador

    private Animator anim; // Referencia al Animator del jugador
    private Rigidbody rb; // Referencia al Rigidbody del jugador
    private bool isGrounded; // Variable para comprobar si el jugador est� en el suelo

    void Start()
    {
        anim = GetComponent<Animator>(); // Obtenemos la referencia al Animator del jugador
        rb = GetComponent<Rigidbody>(); // Obtenemos la referencia al Rigidbody del jugador
    }

    void Update()
    {
        // Obtener la entrada de movimiento del jugador (horizontal y vertical)
        float moveX = Input.GetAxis("Horizontal");
        float moveZ = Input.GetAxis("Vertical");

        // Calcular la velocidad de movimiento en cada eje
        Vector3 move = new Vector3(moveX, 0f, moveZ) * moveSpeed * Time.deltaTime;

        // Aplicar la velocidad de movimiento al Rigidbody del jugador
        rb.MovePosition(transform.position + move);

        // Comprobar si el jugador est� en el suelo
        isGrounded = Physics.Raycast(transform.position, Vector3.down, 0.1f);

        // Si el jugador est� en el suelo y se pulsa el bot�n de saltar, aplicamos una fuerza vertical al Rigidbody del jugador
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        // Obtener la entrada de giro del jugador
        float turn = Input.GetAxis("Horizontal");

        // Calcular la velocidad de giro del jugador
        Vector3 rotation = new Vector3(0f, turn * turnSpeed * Time.deltaTime, 0f);

        // Aplicar la rotaci�n al transform del jugador
        transform.Rotate(rotation);

        // Actualizar las variables del Animator del jugador
        anim.SetFloat("MoveSpeed", move.magnitude);
        anim.SetBool("IsGrounded", isGrounded);
    }
}